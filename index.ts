import { Component, Input, NgModule } from '@angular/core';

/**
 * Águia Map Component
 *
 * Google Map Component for Ionic
 *
 * @example
 * ``` html
 * <zoo-map width="640" height="640" zoom="8" origin="-12.9730401,-38.50230399999998" destination="-12.9730401,-38.50230399999998"></zoo-map>
 * ```
 */
@Component({
  selector: 'zoo-map',
  template: `<div class="cover" [ngStyle]="{'background-image': 'url(https://maps.googleapis.com/maps/api/staticmap?markers=color:green%7Clabel:S%7C' + origin.split(',')[0] + ',' + origin.split(',')[1] + '&markers=color:red%7Clabel:C%7C' + destination.split(',')[0] + ',' + destination.split(',')[1] + '&size=' + width + 'x' + height + '&zoom=' + zoom + '&maptype=roadmap'}"></div>`
})
export class MapComponent {

  /**
   * @Input {string} Latitude and longitude, separated by a comma. Default: "-12.9730401,-38.50230399999998"
   */
  @Input() origin: string = "-12.9730401,-38.50230399999998";

  /**
   * @Input {string} Latitude and longitude, separated by a comma. Default: "-12.9730401,-38.50230399999998"
   */
  @Input() destination: string = "-12.9730401,-38.50230399999998";

  /**
   * @Input {string} Image width. Default: '640'
   */
  @Input() width: string = '640';

  /**
   * @Input {string} Image height. Default: '640'
   */
  @Input() height: string = '640';

  /**
   * @Input {string} Image zoom. Default: '12'
   */
  @Input() zoom: string = '12';

}

@NgModule({
  declarations: [MapComponent],
  exports: [MapComponent]
})
export class MapComponentModule {}
