[![npm](https://img.shields.io/npm/v/@ioniczoo/eagle-map-component.svg)](https://www.npmjs.com/package/@ioniczoo/eagle-map-component)
[![npm](https://img.shields.io/npm/dt/@ioniczoo/eagle-map-component.svg)](https://www.npmjs.com/package/@ioniczoo/eagle-map-component)
[![npm](https://img.shields.io/npm/l/@ioniczoo/eagle-map-component.svg?style=flat-square)](https://www.npmjs.com/package/@ioniczoo/eagle-map-component)

<img src="https://gitlab.com/IonicZoo/eagle-map-component/raw/master/img.jpg" width="20%" height="auto" alt="eagle" title="eagle">

# Águia Map Component

Map Component for Ionic: The eagle, with its panoramic view, provides a world map for you, through a Component.

## Install

```bash
npm install @ioniczoo/eagle-map-component --save
```

## Import

```ts
...
import { MapComponent } from '@ioniczoo/eagle-map-component';

@NgModule({
  declarations: [
    HomePage,
    MapComponent
  ],
  imports: [
    FormatPipeModule,
    IonicPageModule.forChild(HomePage),
  ],
})
export class HomePageModule {}
```

## Examples

```html
<zoo-map width="640" height="640" zoom="8" origin="-12.9730401,-38.50230399999998" destination="-12.9730401,-38.50230399999998"></zoo-map>
```

## Author

[André Argôlo](https://argolo.gitlab.io)

## Contribute

[Create issues and request pull-request.](https://gitlab.com/IonicZoo/eagle-map-component/blob/master/CONTRIBUTING.md)

## License

[GNU General Public License v3.0](https://gitlab.com/IonicZoo/eagle-map-component/blob/master/LICENSE)
